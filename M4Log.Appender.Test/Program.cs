﻿using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M4Log.Appender.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                XmlConfigurator.ConfigureAndWatch(new FileInfo("log4net.config"));
                Logger.Log.InfoFormat("Test started");
            }
            catch (Exception e)
            {
                Logger.Log.Error(e);
            }

            var i = 0;
            //while (i<1000)
            //{
            //    Logger.Log.Debug($"message {i++}");
            //}
            Console.ReadLine();
        }
    }
}
