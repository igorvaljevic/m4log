﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M4Log.Appender.Test
{
    internal static class Logger
    {
        /// <summary>
        /// Log object.
        /// </summary>
        public static readonly ILog Log = LogManager.GetLogger("M4LogAppender_TestLogger");
    }
}
