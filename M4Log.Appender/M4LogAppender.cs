﻿using log4net.Appender;
using log4net.Core;
using M4Log.Appender.Models;
using M4Log.Appender.Senders;
using System;
using System.Collections.Generic;

namespace M4Log.Appender
{
    public class M4LogAppender : AppenderSkeleton
    {
        public string EnpointType { get; set; }
        public string EnpointUrl { get; set; }
        public string ProjectKey { get; set; }
        public string UserKey { get; set; }

        protected override void Append(LoggingEvent[] loggingEvents)
        {
            if (loggingEvents == null) return;

            var m4LoggingEventDtos = new List<M4LoggingEventDto>();
            var endpointUri = new Uri(EnpointUrl);
            var senderProxy = new SenderProxy(endpointUri, EnpointType, UserKey, ProjectKey);

            foreach (var loggingEvent in loggingEvents)
            {
                List<KeyValuePair<string, string>> properties = null;
                var m4LoggingEventDto = new M4LoggingEventDto(loggingEvent);

                if (loggingEvent.Properties.Count > 0)
                {
                    properties = new List<KeyValuePair<string, string>>();
                    foreach (var key in loggingEvent.Properties.GetKeys())
                    {
                        properties.Add(new KeyValuePair<string, string>(key, loggingEvent.Properties[key].ToString()));
                    }
                    m4LoggingEventDto.Properties = properties;
                }
                m4LoggingEventDtos.Add(m4LoggingEventDto);
            }
            senderProxy.Send(m4LoggingEventDtos);
        }

        protected override void Append(LoggingEvent loggingEvent)
        {
            if (loggingEvent == null) return;

            List<KeyValuePair<string, string>> properties = null;
            var m4LoggingEventDto = new M4LoggingEventDto(loggingEvent);

            if (loggingEvent.Properties.Count > 0)
            {
                properties = new List<KeyValuePair<string, string>>();
                foreach (var key in loggingEvent.Properties.GetKeys())
                {
                    properties.Add(new KeyValuePair<string, string>(key, loggingEvent.Properties[key].ToString()));
                }
                m4LoggingEventDto.Properties = properties;
            }
            var endpointUri = new Uri(EnpointUrl);
            var senderProxy = new SenderProxy(endpointUri, EnpointType, UserKey, ProjectKey);
            senderProxy.Send(m4LoggingEventDto);
        }
    }
}