﻿using log4net.Core;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace M4Log.Appender.Models
{
    [DataContract]
    public class M4LoggingEventDto : IM4LoggingEvent
    {
        public M4LoggingEventDto(LoggingEvent loggingEvent)
        {
            Domain = loggingEvent.Domain;
            ExceptionObject = loggingEvent.ExceptionObject?.ToString();
            Identity = loggingEvent.Identity;
            Level = loggingEvent.Level;
            LocationInformation = loggingEvent.LocationInformation?.FullInfo;
            LoggerName = loggingEvent.LoggerName;
            MessageObject = loggingEvent.MessageObject.ToString();
            RenderedMessage = loggingEvent.RenderedMessage;
            ThreadName = loggingEvent.ThreadName;
            TimeStamp = loggingEvent.TimeStamp.ToUniversalTime().ToString("o", System.Globalization.CultureInfo.InvariantCulture);
            UserName = loggingEvent.UserName;
        }

        [DataMember]
        public string Domain { get; set; }

        [DataMember]
        public string ExceptionObject { get; set; }

        [DataMember]
        public string Fix { get; set; }

        [DataMember]
        public string Identity { get; set; }

        [DataMember]
        public Level Level { get; set; }

        [DataMember]
        public string LocationInformation { get; set; }

        [DataMember]
        public string LoggerName { get; set; }

        [DataMember]
        public string MessageObject { get; set; }

        [DataMember]
        public IEnumerable<KeyValuePair<string, string>> Properties { get; set; }

        [DataMember]
        public string RenderedMessage { get; set; }

        [DataMember]
        public string Repository { get; set; }

        [DataMember]
        public string ThreadName { get; set; }

        [DataMember]
        public string TimeStamp { get; set; }

        [DataMember]
        public string UserName { get; set; }
    }
}