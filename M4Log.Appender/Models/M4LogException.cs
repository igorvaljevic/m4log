﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M4Log.Appender.Models
{
    public class M4LogException : Exception
    {

        public M4LogException() :
            base()
        {

        }

        public M4LogException(string message) :
            base(message)
        {

        }

        public M4LogException(string message, Exception innerException) :
            base(message, innerException)
        {

        }
    }
}
