﻿using System.Net;

namespace M4Log.Appender.Models
{
    public interface IM4Response
    {
        string Content { get; set; }
        HttpStatusCode StatusCode { get; set; }
    }
}