﻿using M4Log.Appender.Models;
using System;

namespace M4Log.Appender.Senders
{
    internal static class SenderFactory
    {
        public static ISender GetSender(Uri endpointUri, string senderType, string userKey, string projectKey)
        {
            ISender response;
            if (String.IsNullOrEmpty(userKey))
            {
                throw new M4LogException($"{nameof(userKey)} is mandatory.");
            }
            if (String.IsNullOrEmpty(projectKey))
            {
                throw new M4LogException($"{nameof(projectKey)} is mandatory.");
            }

            switch (senderType)
            {
                case "restApi":
                    response = new RestApiSender(endpointUri, userKey, projectKey);
                    break;

                default:
                    throw new M4LogException($"Unsupported {nameof(senderType)}: {senderType}");
            }
            return response;
        }
    }
}