﻿using M4Log.Appender.Models;
using M4Log.Appender.Senders;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace M4Log.Appender
{
    internal class RestApiSender : ISender
    {
        private HttpClient _client;

        public RestApiSender(Uri endpointUri,string userKey, string projectKey)
        {
            _client = new HttpClient();
            _client.BaseAddress = endpointUri;
            _client.DefaultRequestHeaders.Add("userKey", userKey);
            _client.DefaultRequestHeaders.Add("loggerKey", projectKey);
        }

        public void Send(M4LoggingEventDto logEvent)
        {
            Send(new M4LoggingEventDto[] { logEvent });
        }

        public async void Send(IEnumerable<M4LoggingEventDto> logEvents)
        {
            var response = await _client.PostAsJsonAsync("/api/logs", logEvents);
            response.ToString();
        }
    }
}