﻿using M4Log.Appender.Models;
using System;
using System.Collections.Generic;

namespace M4Log.Appender.Senders
{
    public class SenderProxy
    {
        private ISender _sender;

        public SenderProxy(Uri enpointUri, string enpointType, string userKey, string projectKey)
        {
            _sender = SenderFactory.GetSender(enpointUri, enpointType, userKey, projectKey);
        }

        internal void Send(M4LoggingEventDto m4LoggingEventDto)
        {
            _sender.Send(m4LoggingEventDto);
        }

        internal void Send(IEnumerable<M4LoggingEventDto> m4LoggingEventDtos)
        {
            _sender.Send(m4LoggingEventDtos);
        }
    }
}