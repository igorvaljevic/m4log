﻿using M4Log.Appender.Models;
using System.Collections.Generic;

namespace M4Log.Appender.Senders
{
    internal interface ISender
    {
        void Send(M4LoggingEventDto logEvent);

        void Send(IEnumerable<M4LoggingEventDto> logEvent);
    }
}